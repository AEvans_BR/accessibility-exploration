package com.bottlerocket.accessibilityexploration

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_geometry.view.*

class GeometryAdapter(private val geometries: ArrayList<Geometry>) :
    RecyclerView.Adapter<GeometryAdapter.GeometryHolder>() {

    override fun getItemCount(): Int = geometries.size

    override fun onBindViewHolder(holder: GeometryHolder, position: Int) = holder.bind(geometries[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeometryHolder =
        GeometryHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_geometry, parent, false))

    inner class GeometryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(geometry: Geometry) {
            itemView.apply {
                geometricTitle.text = geometry.name
                body.text = geometry.description
                geometricImage.setImageDrawable(itemView.context.getDrawable(geometry.imgId))
                geometricImage.contentDescription = geometry.imgContentDescription

                geometricSwitchText.setText("Subscribe to ${geometry.name}!")
                geometricSwitch.setOnCheckedChangeListener { _, boolean ->
                    if (boolean) {
                        Toast.makeText(
                            itemView.context,
                            "Subscribed to the ${geometry.name}",
                            Toast.LENGTH_SHORT
                        ).show()
                        geometricSwitchText.setText("Subscribed!")
                    } else {
                        Toast.makeText(
                            itemView.context,
                            "Unsubscribed ${geometry.name} updates!",
                            Toast.LENGTH_SHORT
                        ).show()
                        geometricSwitchText.setText("Subscribe to ${geometry.name}!")
                    }
                }

                infoButton.setOnClickListener {
                    Toast.makeText(
                        itemView.context,
                        "More info",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}